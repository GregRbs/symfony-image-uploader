<?php

declare(strict_types=1);

namespace GregRbs\ImageUploaderBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ImageUploaderBundle extends Bundle
{
}