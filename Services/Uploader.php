<?php

declare(strict_types=1);

namespace GregRbs\ImageUploaderBundle\Services;

use Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use function array_search;
use function imagecreatefromjpeg;
use function imagecreatefrompng;
use function imagejpeg;
use function is_file;
use function sha1_file;
use function unlink;

class Uploader
{
    public static $UPLOAD_DIR = '';
    public static $QUALITY = 60;
    private const MAX_FILE_SIZE = 2E6;
    private const AUTHORIZED_MIME_TYPE = [
        'jpg'  => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'png'  => 'image/png'
    ];

    /**
     * @param UploadedFile $srcFile
     * @param $destinationDir
     * @return string
     * @throws Exception
     */
    public static function uploadImage($srcFile, $destinationDir)
    {
        if (empty($srcFile) || empty($destinationDir)) {
            return;
        }
        if (! $srcFile->isValid()) {
            $maxFileSize = self::MAX_FILE_SIZE / 1E6;
            throw new Exception("Fichier non valide. Vérifiez la taille (max: $maxFileSize Mo) et
             l'extension du fichier.");
        }

        $fileErrors  = $srcFile->getError();
        $fileTmpName = $srcFile->getPathname();
        $fileSize    = $srcFile->getSize();
        $fileType    = $srcFile->getMimeType();

        self::checkUploadErrors($fileErrors);
        $extension = self::checkMimeType($fileType);
        self::checkFileSize($fileSize);
        return self::compressAndSaveFile($fileTmpName, $extension, $destinationDir);
    }

    /**
     * @param $fileErrors
     * @throws Exception
     */
    private static function checkUploadErrors($fileErrors)
    {
        switch ($fileErrors) {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new Exception('Aucun fichier envoyé');
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                throw new Exception('Fichier trop volumineux');
            default:
                throw new Exception('Erreur inconnue, veuillez réessayer');
        }
    }

    /**
     * @param string $fileType
     * @return false|int|string
     * @throws Exception
     */
    private static function checkMimeType($fileType)
    {
        $extension = array_search($fileType, self::AUTHORIZED_MIME_TYPE, true);
        if ($extension === false) {
            throw new Exception("Type de fichier non autorisé");
        } else {
            return $extension;
        }
    }

    /**
     * @param $fileSize
     * @throws Exception
     */
    private static function checkFileSize($fileSize)
    {
        if ($fileSize > self::MAX_FILE_SIZE) {
            throw new Exception("Fichier trop volumineux");
        }
    }

    /**
     * @param $file
     * @param $extension
     * @param string $destinationDir
     *
     * @throws Exception
     */
    private static function compressAndSaveFile($file, $extension, $destinationDir) : string
    {
        switch ($extension) {
            case 'jpg':
            case 'jpeg':
                $image = imagecreatefromjpeg($file);
                break;
            case 'png':
                $image = imagecreatefrompng($file);
                break;
            default:
                throw new Exception("Mauvais type de fichier");
        }

        $newFileName = sha1_file($file);
        $uploadDir   = self::$UPLOAD_DIR;
        $destination = "$uploadDir/$destinationDir/$newFileName.$extension";
        if (strtolower($extension) === 'png') {
            // Convert the quality from 0 to 100 in the compression level from 0 to 9
            $pngCompression = (int) round(((100 - self::$QUALITY) / 11));
            imagealphablending($image, false);
            imagesavealpha($image, true);
            imagepng($image, $destination, $pngCompression);
        } else {
            imagejpeg($image, $destination, self::$QUALITY);
        }
        imagedestroy($image);
        unlink($file);
        return "$newFileName.$extension";
    }

    public static function imageDelete($path) : void
    {
        if ($path[0] !== '/') {
            $path = '/' . $path;
        }
        $fullPath = self::$UPLOAD_DIR . $path;

        if (is_file($fullPath)) {
            unlink($fullPath);
        }
    }
}
